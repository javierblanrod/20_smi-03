#!/bin/bash

# Parametros de entrada
#	$1 nombre del video sin extension
#	$2 extension del video


# Separamos las pistas de audio y video
mkdir  gpac/$1
chmod 777 gpac/$1
ffmpeg -i archivos/$1.$2 -an -c:v copy gpac/$1/$1_v.mp4
ffmpeg -i archivos/$1.$2 -vn -c:a copy gpac/$1/$1_a.mp4

# Generamos las calidades del video (3)
ffmpeg -i gpac/$1/$1_v.mp4 -c:v libx264 -r 24 -g 24 -b:v 1000k -maxrate 1000k -bufsize 2000k gpac/$1/$1_1000k.mp4
ffmpeg -i gpac/$1/$1_v.mp4 -c:v libx264 -r 24 -g 24 -b:v 500k -maxrate 500k -bufsize 1000k gpac/$1/$1_500k.mp4
ffmpeg -i gpac/$1/$1_v.mp4 -c:v libx264 -r 24 -g 24 -b:v 250k -maxrate 250k -bufsize 500k gpac/$1/$1_250k.mp4

# Generamos el manifiesto
MP4Box -dash 5000 -profile onDemand -out  gpac/$1/$1.mpd gpac/$1/$1_a.mp4 gpac/$1/$1_1000k.mp4 gpac/$1/$1_500k.mp4 gpac/$1/$1_1000k.mp4

