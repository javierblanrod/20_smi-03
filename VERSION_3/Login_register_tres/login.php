<?php

    session_start();

    if(isset($_SESSION['userlogin'])){   // Si la sesion esta empezada
        hearder("Location: login_index.php");  // Redirect to
    }
?>



<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://kit.fontawesome.com/df725c4910.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/login_styles.css">

</head>
<body>

    <div class="container h-100">
        <div class="d-flex justify-content-center h-100">
            <div class="user_card">   
                <div class="d-flex justify-content-center">
                    <!-- LOGO -->
                    <div class="brand_logo_container">
                        <img src="img/logo.png" class="brand_logo" alt="TEXTO LOGO">
                    </div>
                </div> 
                <div class="d-flex justify-content-center form_container">
                     <form action="jslogin.php" method="post"> 
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fas fa-user"></i> <!-- importamos de font awsome -->
                                    </span>
                                    <input type="text" name="firstname" id="firstname" class="form-control input_user" required>
                                </div>
                            </div>

                            <div class="input-group mb-2">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fas fa-key"></i> <!-- importamos de font awsome -->
                                    </span>
                                    <input type="password" name="password" id="password" class="form-control input_pass" required>
                                </div>
                            </div>

                                        <!-- Log in button con estilo personalizado -->
                            <div class="d-flex justyfy-content-center mt-3 login_container">
                         <!--       <button type="submit" name="login" id="login" class="btn login_btn"> Login </button>   -->
				<input class="btn login_btn" type="submit" id="login" name="login" value="Login"> 
                            </div>
                    </form>
                </div>
                             <!-- Extra -->
                <div class="mt-4">
                        <!-- No tiene cuenta : DEBERIA FUNCIONAR-->
                    <div class="d-flex justify-content-center links"> 
                        Don't have an account? <a href="registration.php" class="ml-2" style="color:rgb(175, 183, 231)">Sing Up</a>
                    </div>
           <!--</div>-->
                </div>


            </div>
        </div>
    </div>

</body>
</html>
