<?php

    session_start();

    if(!isset($_SESSION['userlogin'])){   // Si la sesion NO esta empezada
        hearder("Location: login.php");   // Redirect to login.php
    }

    if(isset($_GET['logout'])){    // Si logout esta activa destruyo la sesion
        session_destroy();
        unset($_SESSION);
        header("Location: login.php");
    }
?>

<p>ESTO SERIA LA PAGINA DE INICIO UNA VEZ QUE HAS INICIADO SESIÓN</p>


<a href="index.php?logout=true">Logout</a>