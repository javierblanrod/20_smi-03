<?php

ob_start();
    session_start();
if(isset($_SESSION['firstname'])){   // Si la sesion esta empezada
    header("Location:faq_registrado.php");  // Redirect to
}

    //$userlogin = $_SESSION['usuario'];
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <script src="https://kit.fontawesome.com/df725c4910.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="css/Pagina_inicio.css">
        <link rel="stylesheet" href="css/Template-menu.css">
        <link rel="stylesheet" href="css/templatemo-faq.css">

        <title>Hola mundo! - NOMBRE PESTAÑA BUSCADOR</title>
    </head>

    <body>
        <header class="showcase">
            <div class="showcase-top">

                <img src="img/logo.png" alt="HolaMundo">
                <a href="Login_register_tres/login.php" class="btn btn-rounded">Log in</a>
		</div>
                <div class="showcase-content">
            <h1>Plataforma de vídeos turísticos</h1>
            <p></p>
            <a href="index.php" class="boton-medio">Volver al inicio</a>
        </div>
        </header>

         <!--contenido de las preguntas y respuestas-->
        <div class="container tm-container-2">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="tm-welcome-text">Preguntas típicas</h2>
                </div>
            </div>
           
            <div class="row tm-section-mb">
                <div class="col-lg-12">

                    <!--1-->
                    <div class=" tm-timeline-item">
                        <div class="tm-timeline-item-inner">
                            <img src="img/img-01.jpg" alt="Image" class="rounded-circle tm-img-timeline">
                            <div class="tm-timeline-connector">
                                <p class="mb-0">&nbsp;</p>
                            </div>
                            <div class="tm-timeline-description-wrap">
                                <div class="tm-bg-dark tm-timeline-description">
                                    <h3 class="tm-text-green tm-font-400">¿Con qué utilidad se ha creado esta página?</h3>
                                    <p>Con esta página pretendemos proporcinar un sitio para conocer otros lugares del mundo y ver opciones de destinos</p>
                                </div>
                            </div>
                        </div>

                        <div class="tm-timeline-connector-vertical"></div>
                    </div>

                    <!--2-->
                    <div class="tm-timeline-item">
                        <div class="tm-timeline-item-inner">
                            <img src="img/img-02.jpg" alt="Image" class="rounded-circle tm-img-timeline">
                            <div class="tm-timeline-connector">
                                <p class="mb-0">&nbsp;</p>
                            </div>
                            <div class="tm-timeline-description-wrap">
                                <div class="tm-bg-dark-light tm-timeline-description">
                                    <h3 class="tm-text-cyan tm-font-400">¿Puedo subir videos o acceder a contenido si no estoy registrado?</h3>
                                    <p>Si no esta registrado sólamente tiene acceso a los videos de muestra y no puede subir contenidos.</p>
                                </div>
                            </div>
                        </div>
                        <div class="tm-timeline-connector-vertical"></div>
                    </div>

                    <!--3-->
                    <div class="tm-timeline-item">
                        <div class="tm-timeline-item-inner">
                            <img src="img/img-03.jpg" alt="Image" class="rounded-circle tm-img-timeline">
                            <div class="tm-timeline-connector">
                                <p class="mb-0">&nbsp;</p>
                            </div>
                            <div class="tm-timeline-description-wrap">
                                <div class="tm-bg-dark tm-timeline-description">
                                    <h3 class="tm-text-yellow tm-font-400">¿ Por que no estan todos los continentes? </h3>
                                    <p>La pagina se esta desarrollando y no descartamos añadirlos en un futuro</p>
                                </div>
                            </div>
                        </div>
                        <div class="tm-timeline-connector-vertical"></div>
                    </div>

                    <!--4-->
                    <div class="tm-timeline-item">
                        <div class="tm-timeline-item-inner">
                            <img src="img/img-04.jpg" alt="Image" class="rounded-circle tm-img-timeline">
                            <div class="tm-timeline-connector">
                                <p class="mb-0">&nbsp;</p>
                            </div>
                            <div class="tm-timeline-description-wrap">
                                <div class="tm-bg-dark-light tm-timeline-description">
                                    <h3 class="tm-text-orange tm-font-400">¿Puedo descargar contenido?</h3>
                                    <p>No, el contenido de nuestra página es unicamente para visualización</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <footer class="footer">
                <p>Enlaces de interés</p>
                <div class="footer-cols">
                    <ul>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="informacion.php">Informacion corporativa</a></li>
                        <br>
                        <br>
                        <br>
                            <p>&#169; 2020 <a href="https://www.gijon.es/">HelloWorld!</a></p>
                       
                    </ul>
                </div>
            </footer>



    </body>

</html>
