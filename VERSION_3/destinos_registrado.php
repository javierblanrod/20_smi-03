<?php

session_start();

if(!isset($_SESSION['firstname'])){   // Si la sesion esta empezada
    header("Location: index.php");  // Redirect to
}

    //$userlogin = $_SESSION['usuario'];
?>

<?php
    require('register_config.php');

    $dir="miniaturas";  // Directorio en el que estan todos los videos subidos 
                    // Si tenemos una imagen de cada video sacada se comentaran las modificaciones --> directorio
 

    //$sqlVideo_africa = "SELECT id, nombre, extension, id_user FROM subirVideos WHERE continente='africa'";
    //$sqlVideo_america = "SELECT id, nombre, extension, id_user FROM subirVideos WHERE continente='america'";

    $sqlVideo_oceania = "SELECT id, nombre, extension, id_user FROM subirVideos  WHERE   continente='America'"; // continente='UNO'"; ARREGLAR CONDICION
    $videosOceania = mysqli_query($conexion,$sqlVideo_oceania); 
    if ($videosOceania){
           // echo "<br> Datos recogidos de la base usuarios correctamente <br> ";
        } else {
            echo "<br>Error recoger datos de la tabla (Oceania) <br>";
    };


    $sqlVideo_asia = "SELECT id, nombre, extension, id_user FROM subirVideos WHERE continente='Asia'"; //continente='asia'";
    $videosAsia = mysqli_query($conexion,$sqlVideo_asia); 
    if ($videosAsia){
            //echo "<br> Datos recogidos de la base usuarios correctamente <br> ";
        } else {
            echo "<br>Error recoger datos de la tabla (Asia) <br>";
    };

    $sqlVideo_europa = "SELECT id, nombre, extension, id_user FROM subirVideos WHERE continente='Europa'"; //continente='europa'";
    $videosEuropa = mysqli_query($conexion,$sqlVideo_europa); 
    if ($videosEuropa){
            //echo "<br> Datos recogidos de la base usuarios correctamente <br> ";
        } else {
            echo "<br>Error recoger datos de la tabla (Europa) <br>";
    };
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://kit.fontawesome.com/df725c4910.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/Pagina_inicio.css">
    <link rel="stylesheet" href="css/Template-menu.css">
    <title>Hola mundo! - NOMBRE PESTAÑA BUSCADOR</title>
</head>

<body>
    <header class="showcase">
        <div class="showcase-top">
            <img src="img/logo.png" alt="HolaMundo">
		<a href="Login_register_tres/logout.php" class="btn btn-rounded">Salir</a>
        </div>
        <div class="showcase-content">
            <h1>Plataforma de vídeos turísticos</h1>
            <a href="index.php" class="boton-medio">Volver al inicio</a>
            <p></p>
        </div>
    </header>

    <section class="tabs">
        <div class="container">
            <!--para que los tabs esten restringidos de tamaño-->
            <!--1 por cada tab-->
            <div id="#tab-1" class="tab-item tab-border">
                <!--id para meterlo en js; tap border por defecto para tener el selecctor de tabs -->
                <i class="fas fa-globe-europe fa-3x"></i>
                <!--icono-->
                <p class="hide-sm">Europa</p>
                <!--hide sm para que no se muestre en pantallas pequeñas-->
            </div>

            <div id="#tab-2" class="tab-item">
                <i class="fas fa-globe-americas fa-3x"></i>
                <!--<i class="fas fa-door-open fa-3x"></i>  CAMBIO -->
                <p class="hide-sm">America</p>
            </div>

            <div id="#tab-3" class="tab-item">
                <i class="fas fa-globe-asia fa-3x"></i>
                <!--<i class="fas fa-door-open fa-3x"></i>   CAMBIO-->
                <p class="hide-sm">Asia</p>
            </div>
        </div>
    </section>



    <section class="tab-content">
        <div class="container">
            <!--Contenido del tab 1-->
            <!--uno sera para el texto y otro para la imagen-->

            <div id="tab-1-content" class="tab-content-item show">
                <div class="tab-1-content-top">
                    <p class="text-lg">
                        Usted ha seleccionado Europa
                    </p>
                    <!-- < !--CAMBIO href="" - -> -->
                </div>

                <div class="tab-1-content-bottom">
		    <div>
                        <img src="img/mapaeuropa.jpg" alt="">
                        <p class="text-dark">Mapa de Oceania</p>
                    </div>

                    <?php 
                        if (mysqli_num_rows($videosEuropa) > 0) {  //Vuelvo todos los videos de oceania

                            while($row = mysqli_fetch_assoc($videosEuropa)) {
                                $nombreVideo=$dir."/".$row['nombre'].".png";

				    $idUploader=$row['id_user'];
                                    $sql = "SELECT firstaname, lastname FROM users WHERE id='$idUploader'";
                                    $uploader = mysqli_query($conexion,$sql);
				    $rowUploader = mysqli_fetch_assoc($uploader);
                                    $nombreUploader= $rowUploader['firstaname']." ".$rowUploader['lastname'];

                                echo '<html>';
		                echo '<h4>'.htmlspecialchars($row['nombre']).' </h4>';
                                echo '<a href="playerHTML.php?idVideo='.htmlspecialchars($row['id']).'" title="Reproducir"/>';
                                echo '<img src="'.htmlspecialchars($nombreVideo).'" alt="360">';
		                echo '<p> Video subido por:'.htmlspecialchars($nombreUploader).' </p>';
                                echo '<br></br> <br></br>';
                                echo '</html>';
                            }
                        } else {
                            echo "<br>No hay videos de europa<br>";
                        }
                    ?>
                </div>    
            </div>
            <!--Contenido del tab 2-->
            <div id="tab-2-content" class="tab-content-item">

                <div class="tab-2-content-top">
                    <p class="text-lg">
                        Usted ha seleccionado America
                    </p>
                    <!-- < !--CAMBIO href="" - -> -->
                </div>

                <div class="tab-2-content-bottom">
                    <div>
                        <img src="img/mapaamerica.jpg" alt="">
                        <p class="text-dark">Mapa de America</p>
                    </div>
                        <?php
                            if (mysqli_num_rows($videosAsia) > 0) {  //Vuelvo todos los videos de oceania
                                while($row = mysqli_fetch_assoc($videosAsia)) {

					$idUploader=$row['id_user'];
                                    	$sql = "SELECT firstaname, lastname FROM users WHERE id='$idUploader'";
                                    	$uploader = mysqli_query($conexion,$sql);
                                    	$rowUploader = mysqli_fetch_assoc($uploader);
                                    	$nombreUploader= $rowUploader['firstaname']." ".$rowUploader['lastname'];

                                    $nombreVideo=$dir."/".$row['nombre'].".png";
                        		echo '<html>';
                                	echo '<h4>'.htmlspecialchars($row['nombre']).' </h4>';
                                	echo '<a href="playerHTML.php?idVideo='.htmlspecialchars($row['id']).'" title="Reproducir"/>';
                                	echo '<img src="'.htmlspecialchars($nombreVideo).'" alt="360">';
                                	echo '<p> Video subido por:'.htmlspecialchars($nombreUploader).' </p>';
                                	echo '<br></br> <br></br>';
                               		echo '</html>';
                                }
                            } else {
                                echo "<br>No hay videos de america<br>";
                            }
                        ?>
                </div>
            </div>


            <!--Contenido del tab 3-->
            <div id="tab-3-content" class="tab-content-item">
                <div class="tab-3-content-top">
                    <p class="text-lg">
                        Usted ha seleccionado Asia
                    </p>
                </div>

                <div class="tab-3-content-bottom">
                    <div>
                        <img src="img/mapaasia.jpg" alt="">
                        <p class="text-dark">Mapa de Asia</p>
                    </div>
                        <?php
                            if (mysqli_num_rows($videosOceania) > 0) {  //Vuelvo todos los videos de oceania
                                while($row = mysqli_fetch_assoc($videosOceania)) {

					$idUploader=$row['id_user'];
                            		$sql = "SELECT firstaname, lastname FROM users WHERE id='$idUploader'";
                                    	$uploader = mysqli_query($conexion,$sql);
                                    	$rowUploader = mysqli_fetch_assoc($uploader);
                                    	$nombreUploader= $rowUploader['firstaname']." ".$rowUploader['lastname'];

                                    $nombreVideo=$dir."/".$row['nombre'].".png";
                                    	echo '<html>';
                                	echo '<h4>'.htmlspecialchars($row['nombre']).' </h4>';
                                	echo '<a href="playerHTML.php?idVideo='.htmlspecialchars($row['id']).'" title="Reproducir"/>';
                                	echo '<img src="'.htmlspecialchars($nombreVideo).'" alt="360">';
                                	echo '<p> Video subido por:'.htmlspecialchars($nombreUploader).' </p>';
                                	echo '<br></br> <br></br>';
                                	echo '</html>';
                                }
                            } else {
                                echo "<br>No hay videos de asia<br>";
                            }
                        ?>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer">
        <p>Enlaces de interés</p>
        <div class="footer-cols">
            <ul>
                <li><a href="faq.php">FAQ</a></li>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="informacion.php">Informacion corporativa</a></li>
                <br>
                <br>
                <br>
                    <p>&#169; 2020 <a href="https://www.gijon.es/">HelloWorld!</a></p>
               
            </ul>
        </div>
    </footer>

    <script src="js/Pagina_inicio.js"></script>

</body>

</html>
