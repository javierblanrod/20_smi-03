
const tabItems = document.querySelectorAll('.tab-item'); /*Para todas las clases creadas en el html que llevan tab-item sobre los que se clicka*/
const tabContentItems = document.querySelectorAll('.tab-content-item'); /*Iconos sobre los que se clicka*/

//Seleccionar contenido de los items de los tab
function selectItem(e) {
    removeBorder();
    removeShow();
    //Añadimos bordes al tab actual
    this.classList.add('tab-border');
    //Coger el contenido de DOM
      console.log(this.id)
    const tabContentItems = document.querySelector(`${this.id}-content`);
    // Añadir clase show
    tabContentItems.classList.add('show');
}

function removeBorder() { //Desparece la barra  en los iconos que no estoy pulsando
    tabItems.forEach(item => item.classList.remove('tab-border'));
}
tabItems.forEach(item => item.addEventListener('click', selectItem)); /*Para cuando se haga click en los iconos*/

function removeShow() { //Desparece la barra  en los iconos que no estoy pulsando
    tabContentItems.forEach(item => item.classList.remove('show'));
}