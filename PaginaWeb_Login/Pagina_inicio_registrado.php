<?php
//ob_start();
session_start();
if(!isset($_SESSION['firstname'])){   // Si la sesion esta empezada
    header("Location: Pagina_inicio.php");  // Redirect to
}
    //$userlogin = $_SESSION['usuario'];
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://kit.fontawesome.com/df725c4910.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/Pagina_inicio.css">
    <title>Hola mundo! - NOMBRE PESTAÑA BUSCADOR</title>
</head>

<body>
    <header class="showcase">
        <div class="showcase-top">

            <img src="img/logo.png" alt="HolaMundo">
            <a href="Login_register_tres/logout.php" class="btn btn-rounded">Salir</a>
        </div>
        <div class="showcase-content">
            <h1>Plataforma de vídeos turísticos</h1>
            <p></p>

        </div>


    </header>

    <section class="tabs">
        <div class="container">
            <!--para que los tabs esten restringidos de tamaño-->
            <!--1 por cada tab-->
            <div id="#tab-1" class="tab-item tab-border">
                <!--id para meterlo en js; tap border por defecto para tener el selecctor de tabs -->
                <i class="fas fa-train fa-3x"></i>
                <!--icono-->
                <p class="hide-sm">Sobre nosotros</p>
                <!--hide sm para que no se muestre en pantallas pequeñas-->
            </div>

            <div id="#tab-2" class="tab-item">
                <i class="fas fa-mountain fa-3x"></i>
                <!--<i class="fas fa-door-open fa-3x"></i>  CAMBIO -->
                <p class="hide-sm">Galeria de vídeos</p>
            </div>

            <div id="#tab-3" class="tab-item">
                <i class="fas fa-plane fa-3x"></i>
                <!--<i class="fas fa-door-open fa-3x"></i>   CAMBIO-->
                <p class="hide-sm">Contacto</p>
            </div>
        </div>
    </section>

    <section class="tab-content">
        <div class="container">
            <!--Contenido del tab 1-->
            <!--uno sera para el texto y otro para la imagen-->

            <div id="tab-1-content" class="tab-content-item show">
                <div class="tab-1-content-inner">

                    <div>
                        <p class="text-lg">
                            Sobre nosotros:
                        </p>
                        <!-- <a href="#" class="btn btn-lg">Texto del tab 1</a> -->
                    </div>
                    <div>
                        <p>
                            Somos un grupo de alumnos del Grado en Ingeniería en Tecnologias y Servicios de Telecomunicación. Estamos desarrollando una pagina para la subida de vídeos promocionales de ámbito turístico.

                        </p>

                    </div>

                    <!-- <img src="img/cancelar.jpg" alt="" /> -->

                </div>
            </div>

            <!--CONTENIDO DEL TAB 2-->
            <!--tendra 2 secciones: texto de arriba y lo de abajo-->
            <!--la parte de abajo empieza en tab-2-content-bottom-->
            <!--dentro del content-bottom habra 3 divs, cada uno con 1 img y 2 parrafos-->
            <!--el 'texto de arriba' sera blanco (text md lo hace + grande) y el de abajo oscuro-->

            <div id="tab-2-content" class="tab-content-item">
                <div class="tab-2-content-top">
                    <p class="text-lg">
                        Vídeos subidos por los integrantes del grupo:
                    </p>
                    <!--     <a href="#" class="btn btn-lg">Texto del boton</a>     -->
                    <!-- < !--CAMBIO href="" - -> -->
                </div>

                <div class="tab-2-content-bottom">
                    <div>
                        <p class="text-dark">Vídeo de Carmen</p>
                        <video width="320" height="240" controls>
                        <source src="videos/africa.mp4" type="video/mp4">
                    </div>

                    <div>
                        <p class="text-dark">Vídeo de Pablo</p>
                        <video width="320" height="240" controls>
                        <source src="img/video2.mp4" type="video/mp4">
                    </div>

                    <div>
                        <p class="text-dark">Vídeo de Javi</p>    
                        <video width="320" height="240" controls>
                        <source src="videos/washington.mp4" type="video/mp4">
                    </div>
                </div>
            </div>



            <!--CONTENIDO DEL TAB 3-->
            <div id="tab-3-content" class="tab-content-item">
                <div class="text-center">
                    <p class="text-lg">Contacto de los desarrolladores: </p>
                    <!-- <a href="#" class="btn btn-lg">Watch Free for 30 Days</a> -->
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Correo</th>
                            <th>Teléfono</th>
                        </tr>

                    </thead>
                    <tbody>
                        <tr>
                            <td>Pablo Mateos Gil</td>
                            <td>uo265421@uniovi.es </td>
                            <td>689971880 </td>

                        </tr>
                        <tr>
                            <td>Javier Blanco Rodríguez</td>
                            <td>uo251959@uniovi.es </td>
                            <td></td>

                        </tr>
                        <tr>
                            <td>Carmen Sánchez García </td>
                            <td>uo263657@uniovi.es </td>
                            <td> </td>
                        </tr>


                    </tbody>
                </table>
                <div>
                    <br>
                    <br>
                    <br>
                    <iframe style="float: left;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2892.9480052180843!2d-5.636751784232149!3d43.52427696877533!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd367b6bf5530701%3A0xd5c275a10af21bac!2sEscuela%20Polit%C3%A9cnica%20de%20Ingenier%C3%ADa%20de%20Gij%C3%B3n!5e0!3m2!1ses!2ses!4v1585261095989!5m2!1ses!2ses"
            width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"  ></iframe>
        
            </div>
            </div>
        </div>
    </section>

    <footer class="footer">
        <p>Enlaces de interés</p>
        <div class="footer-cols">
            <ul>
                <li><a href="faq.php">FAQ</a></li>
                <li><a href="destinos.php">Destinos por continentes</a></li>
                <li><a href="informacion.php">Información Corporativa</a></li>
                <li><a href="#">Vídeos</a></li>
                <br>
                <br>
                <br>
                    <p>&#169; 2020 <a href="https://www.gijon.es/">HelloWorld!</a></p>
               
            </ul>
        </div>
    </footer>

    <script src="js/Pagina_inicio.js"></script>
</body>

</html>
