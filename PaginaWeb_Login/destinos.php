<?php
ob_start();
session_start();
if(isset($_SESSION['firstname'])){   // Si la sesion esta empezada
    header("Location: destinos_registrado.php");  // Redirect to
}

    //$userlogin = $_SESSION['usuario'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://kit.fontawesome.com/df725c4910.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/Pagina_inicio.css">
    <title>Hola mundo! - NOMBRE PESTAÑA BUSCADOR</title>
</head>

<body>
    <header class="showcase">
        <div class="showcase-top">

            <img src="img/logo.png" alt="HolaMundo">
            <a href="Login_register_tres/login.php" class="btn btn-rounded">Login</a>
        </div>
        <div class="showcase-content">
            <h1>Plataforma de vídeos turísticos</h1>
            <p></p>

        </div>
    </header>



    <section class="tabs">
        <div class="container">
            <!--para que los tabs esten restringidos de tamaño-->
            <!--1 por cada tab-->
            <div id="#tab-1" class="tab-item tab-border">
                <!--id para meterlo en js; tap border por defecto para tener el selecctor de tabs -->
                <i class="fas fa-train fa-3x"></i>
                <!--icono-->
                <p class="hide-sm">Europa</p>
                <!--hide sm para que no se muestre en pantallas pequeñas-->
            </div>

            <div id="#tab-2" class="tab-item">
                <i class="fas fa-mountain fa-3x"></i>
                <!--<i class="fas fa-door-open fa-3x"></i>  CAMBIO -->
                <p class="hide-sm">América</p>
            </div>

            <div id="#tab-3" class="tab-item">
                <i class="fas fa-plane fa-3x"></i>
                <!--<i class="fas fa-door-open fa-3x"></i>   CAMBIO-->
                <p class="hide-sm">Asia</p>
            </div>
        </div>
    </section>



    <section class="tab-content">
        <div class="container">
            <!--Contenido del tab 1-->
            <!--uno sera para el texto y otro para la imagen-->

            <div id="tab-1-content" class="tab-content-item show">
                <div class="tab-1-content-top">
                    <p class="text-lg">
                        Usted ha seleccionado Europa
                    </p>
                    <!-- < !--CAMBIO href="" - -> -->
                </div>

                <div class="tab-1-content-bottom">
                    <div>
                        <img src="img/mapaeuropa.jpg" alt="">
                        <p class="text-dark">Mapa de Europa</p>
                    </div>

                    <div>
                        <video width="320" height="240" controls>
                       <source src="videos/europa.mp4" type="video/mp4">
                    </div>

                    <div>
                        <video width="320" height="240" controls>
                        <source src="videos/europa2.mp4" type="video/mp4">                                
                    </div>
                </div>

                
            </div>


            <!--Contenido del tab 2-->
            <div id="tab-2-content" class="tab-content-item">

                <div class="tab-2-content-top">
                    <p class="text-lg">
                        Usted ha seleccionado América
                    </p>
                    <!-- < !--CAMBIO href="" - -> -->
                </div>

                <div class="tab-2-content-bottom">
                    <div>
                        <img src="img/mapaamerica.jpg" alt="">
                        <p class="text-dark">Mapa de América</p>
                    </div>

                    <div>
                        <video width="320" height="240" controls>
                       <source src="videos/americasur.mp4" type="video/mp4">
                    </div>

                    <div>
                        <video width="320" height="240" controls>
                        <source src="videos/washington.mp4" type="video/mp4">                                
                    </div>

                </div>
            </div>


            <!--Contenido del tab 3-->
            <div id="tab-3-content" class="tab-content-item">
                <div class="tab-3-content-top">
                    <p class="text-lg">
                        Usted ha seleccionado Asia
                    </p>
                </div>

                <div class="tab-3-content-bottom">
                    <div>
                        <img src="img/mapaasia.jpg" alt="">
                        <p class="text-dark">Mapa de Asia</p>
                    </div>

                    <div>
                        <video width="320" height="240" controls>
                       <source src="videos/asia.mp4" type="video/mp4">
                    </div>

                    <div>
                        <video width="320" height="240" controls>
                        <source src="videos/asia2.mp4" type="video/mp4">
                    </div>

                </div>
            </div>
        </div>
    </section>




    <footer class="footer">
        <p>Enlaces de interés</p>
        <div class="footer-cols">
            <ul>
                <li><a href="faq.php">FAQ</a></li>
                <li><a href="destinos.php">Destinos por continentes</a></li>
                <li><a href="informacion.php">Información Corporativa</a></li>
                <li><a href="#">Vídeos</a></li>
                <br>
                <br>
                <br>
                    <p>&#169; 2020 <a href="https://www.gijon.es/">HelloWorld!</a></p>
               
            </ul>
        </div>
    </footer>

    <script src="js/Pagina_inicio.js"></script>
</body>

</html>
