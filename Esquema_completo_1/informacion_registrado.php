<?php
session_start();
if(!isset($_SESSION['firstname'])){   // Si la sesion esta empezada
    header("Location: informacion.php");  // Redirect to
}
    //$userlogin = $_SESSION['usuario'];
?>


<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <script src="https://kit.fontawesome.com/df725c4910.js" crossorigin="anonymous"></script>


        <link rel="stylesheet" href="css/templatemo-inf.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.inf.css">




        <link rel="stylesheet" href="css/Pagina_inicio.css">
        <title>Hola mundo! - NOMBRE PESTAÑA BUSCADOR</title>
    </head>

    <body>
        <header class="showcase">
            <div class="showcase-top">

                <img src="img/logo.png" alt="HolaMundo">
                <a href="Login_register_tres/logout.php" class="btn btn-rounded">Salir</a>
            </div>
            <div class="showcase-content">
                <h1>Plataforma de vídeos turísticos</h1>
            </div>
        </header>

        <!-- Contenido -->

        <section class="section" id="trainers">
            <div class="container">


                <div class="row">
                    <div class="col-lg-6 offset-lg-3">
                        <div class="section-heading">
                            <h2>Desarrolladores</h2>
                            <p>¡Conócenos un poco más!</p>
                        </div>
                    </div>
                </div>


                <div class="row">

                    <!--1-->
                    <div class="col-lg-4">
                        <div class="trainer-item">
                            <div class="image-thumb">
                                <img src="img/first-trainer.jpg" alt="">
                            </div>
                            <div class="down-content">
                                <br>
                                <h5>Estudiante</h5>
                                <h4>Carmen Sánchez</h4>
                                <p>Estudiante de Ingeniería de Telecomunicaciones de tercer año.</p>
 
                            </div>
                        </div>
                    </div>


                    <!--2-->
                    <div class="col-lg-4">
                        <div class="trainer-item">
                            <div class="image-thumb">
                                <img src="img/second-trainer.jpg" alt="">
                            </div>
                            <div class="down-content">
                                <br>
                                <h5>Estudiante</h5>
                                <h4>Pablo Mateos</h4>
                                <p>Estudiante de Ingeniería de Telecomunicaciones de tercer año.</p>
                            </div>
                        </div>
                    </div>

                    <!--3-->
                    <div class="col-lg-4">
                        <div class="trainer-item">
                            <div class="image-thumb">
                                <img src="img/third-trainer.jpg" alt="">
                            </div>
                            <div class="down-content">
                                <br>
                                <h5>Estudiante</h5>
                                <h4>Javier Blanco</h4>
                                <p>Estudiante de Ingeniería de Telecomunicaciones de quinto año.</p>                             
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        
            <footer class="footer">
                <div class="footer-cols">
                    <ul>
                        <li>Enlaces de interés</li>
                        <br>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="destinos.php">Destinos por continentes</a></li>
                        <li><a href="informacion.php">Información Corporativa</a></li>
                        <li><a href="#">Vídeos</a></li>
                        <br>
                        <br>
                        <br>
                            <p>&#169; 2020 <a href="https://www.gijon.es/">HelloWorld!</a></p>
                    
                    </ul>
                </div>
            </footer>

    </body>

</html>
